FROM node:14

WORKDIR /home/node/app

COPY package*.json ./

COPY tsconfig.json ./

RUN npm i
RUN npm i -g nodemon

COPY . .

EXPOSE 4000

CMD ["nodemon", "src/app.ts"]