import * as express from "express";
import * as UserController from "../controllers/UserController";

export const router = express.Router();


router.get('/users', UserController.getListUser) 

router.post('/users', UserController.createUser)