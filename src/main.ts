import * as dotenv from "dotenv";
import * as express from 'express';
import { createConnection } from 'typeorm';
import { join } from "path";
import * as bodyParser from "body-parser";

import { router as api } from "./routers/api";

dotenv.config({ path: join(__dirname,'../.env') });


export const main = async () => {

    const app = express();

    await createConnection({
        "type": "mysql",
        "host": process.env.DB_HOST,
        "port": Number(process.env.DB_PORT),
        "username": process.env.DB_USERNAME,
        "password": process.env.DB_PASSWORD,
        "database": process.env.DB_NAME,
        entities: ['src/models/*.ts']
    })

    app.use(bodyParser.json())

    app.use('/api', api);

    app.listen(4000, () => {
        console.log(`server running on port 4000`);
    });

}