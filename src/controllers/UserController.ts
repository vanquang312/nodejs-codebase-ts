import { Request, Response } from "express";
import { getManager, getRepository } from "typeorm";
import { User } from "../models/User";


export const getListUser = async (req: Request, res: Response) => {

    const userRepository = getRepository(User);
    const users = await userRepository.find();

    res.json(users)

}

export const createUser = async (req: Request, res: Response) => {
    const { first_name, last_name } = req.body
    const userRepository = getRepository(User);
    const user = await userRepository.insert({
        first_name,
        last_name
    });
    res.statusCode = 201
    res.json(user)
    return 
}